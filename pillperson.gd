class_name PillPerson
extends CharacterBody3D

@export var speed := 10.0
@export_range(0.001,60.0) var accel := 30
@export_range(0.1,10.0) var mouse_speed := 4

var _gravity : Vector3
@onready var _window := get_tree().root as Window

func _enter_tree() -> void:
	set_multiplayer_authority(int(str(name)), true)

func _ready() -> void:
	var space := get_viewport().find_world_3d().space
	var grav_len := PhysicsServer3D.area_get_param(space, PhysicsServer3D.AREA_PARAM_GRAVITY) as float
	var grav_dir := PhysicsServer3D.area_get_param(space, PhysicsServer3D.AREA_PARAM_GRAVITY_VECTOR) as Vector3
	_gravity = grav_dir * grav_len

	if is_multiplayer_authority():
		$SpringArm3D/Camera3D.make_current()
		#Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		position = Vector3(10.0*randf(), 0.0, 10.0*randf())
	else:
		$SpringArm3D/Camera3D.clear_current(false)
		$SpringArm3D.queue_free()

func _physics_process(delta: float) -> void:
	if is_multiplayer_authority():
		var camdir := Input.get_last_mouse_velocity()
		rotate(Vector3.UP, (-camdir.x / _window.size.x) * mouse_speed * delta)
		$SpringArm3D.rotate(Vector3.RIGHT, (-camdir.y / _window.size.y) * mouse_speed * delta)
		
		var inputs := Input.get_vector("ui_left","ui_right","ui_up","ui_down").rotated(-rotation.y)
		var velflat := Vector2(velocity.x,velocity.z)
		velflat = velflat.move_toward(inputs*speed,accel*delta)
		velocity = Vector3(velflat.x,velocity.y,velflat.y)
		
		if is_on_floor() and Input.is_action_just_pressed("ui_accept"):
			velocity.y += 10.0

	velocity += _gravity*delta
	move_and_slide()
