extends Node3D

@export var PlayerScene : PackedScene = preload("res://pillperson.tscn")

func _ready() -> void:
	randomize()
	
	multiplayer.server_relay = false

	# TODO: NEVER EVER EVER EEEEEEVEEEEER DO THIS!
	# works tho.
	if not _server(8762):
		_client("127.0.0.1",8762)

	if not multiplayer.is_server():
		return

	if not OS.has_feature("dedicated_server"):
		_spawn(1)

	for id in multiplayer.get_peers():
		_spawn(id)

	multiplayer.peer_connected.connect(_spawn)
	multiplayer.peer_disconnected.connect(func(id: int):
		$Multiplayers.get_node(str(id)).queue_free()
	)

func _spawn(id: int) -> void:
	var me := PlayerScene.instantiate()
	me.name = str(id)
	$Multiplayers.add_child(me)


func _server(port: int) -> bool:
	var peer = ENetMultiplayerPeer.new()
	peer.create_server(port)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		return false
	multiplayer.multiplayer_peer = peer
	return true

func _client(host: String, port: int) -> bool:
	var peer = ENetMultiplayerPeer.new()
	peer.create_client(host, port)
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		return false
	multiplayer.multiplayer_peer = peer
	return true
